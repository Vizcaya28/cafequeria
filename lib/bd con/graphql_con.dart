
import 'dart:io';
import 'package:f_groceries/HomeScreen.dart';
import 'package:f_groceries/main.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class Connect extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    final HttpLink httpLink = 
    HttpLink(uri:"https://cafequeria.appspot.com/");
    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
      GraphQLClient(
        link: httpLink as Link,
        cache: OptimisticCache(
          dataIdFromObject: typenameDataIdFromObject,
        )
      )
    );    
  return GraphQLProvider(
    child: Home_screen(),
    client: client,
  );
  }
}