import 'package:f_groceries/HomeScreen.dart';
import 'package:f_groceries/logind_signup.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Signup_Screen extends StatefulWidget {
  static String tag = 'signup-page';
  @override
  _SignupPageState createState() => new _SignupPageState();
}

class _SignupPageState extends State<Signup_Screen> {
  @override
  Widget build(BuildContext context) {
    final logo = Container(
      alignment: Alignment.center,
      child: new Container(
          width: 200.0,
          height: 200.0,
          decoration: new BoxDecoration(

              shape: BoxShape.circle,border: Border.all(color: Color.fromRGBO(126, 217, 87, 0.9), width: 5.0),
              image: new DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("images/logo.png")
              )
          )),
    );
    final email = TextFormField(

      //keyboardType: TextInputType.number,
      style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.w600),
      autofocus: false,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(

          labelStyle: TextStyle(color: Colors.white,fontSize: 28),
          icon: Icon(Icons.mail,
              color: Colors.white),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          labelText: "E-mail",
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5))),
    );
    final exp = TextFormField(

      //keyboardType: TextInputType.number,
      style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.w600),
      autofocus: false,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(

          labelStyle: TextStyle(color: Colors.white,fontSize: 28),
          icon: Icon(Icons.dialpad,
              color: Colors.white),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          labelText: "Expediente",
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5))),
    );
    final password2 = TextFormField(

      //keyboardType: TextInputType.number,
      style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.w600),
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(

          labelStyle: TextStyle(color: Colors.white,fontSize: 28),
          icon: Icon(Icons.replay,
              color: Colors.white),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5)),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          labelText: "Confirmar contraseña",
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5))),
    );
    final password = TextFormField(
      style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.w600),
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          labelStyle: TextStyle(color: Colors.white,fontSize: 28),
          labelText: 'Contraseña',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          icon: Icon(Icons.lock, color: Colors.white),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Color.fromRGBO(126, 217, 87, 0.8), width: 2.5))),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Home_screen()));
        },
        padding: EdgeInsets.all(12),
        color: Color.fromRGBO(126, 217, 87, 0.9),
        child:
        Text('Registrarse', style: TextStyle(color: Colors.white, fontSize: 20)),
      ),
    );

    final forgotLabel = MaterialButton(
      height: 80,
      color: Color.fromRGBO(204, 31, 0, 0.8),
      //minWidth: 150,
      child: Text(
        'Olvidé mi contrseña',
        textDirection: TextDirection.ltr,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
      ),
      onPressed: () {},
    );
    final registerLabel = MaterialButton(
      height: 80,
      minWidth: 150,
      color: Color.fromRGBO(242, 157, 35, 0.8),
      child: Text(
        'Ingresar',
        textDirection: TextDirection.rtl,
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white, fontSize: 20),
      ),
      onPressed: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>Login_Screen()));
      },
    );

    return Scaffold(
      //backgroundColor: Colors.white,
      body: new Container(

        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("images/food.jpg"),
            fit: BoxFit.cover,
            // colorFilter: ColorFilter.mode(Colors.white, BlendMode.colorBurn)
          ),
        ),
        child: new ListView(
          children: <Widget>[
            logo,
            //SizedBox(height: MediaQuery.of(context).size.height / 4),
            email,
            SizedBox(height: 8.0),
            exp,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 8.0),
            password2,
            SizedBox(height: 24.0),
            loginButton,
          ],
        ),

      ),
      persistentFooterButtons: <Widget>[
        Container(
          height: 80,

          width: MediaQuery.of(context).size.width * 0.47,
          //alignment: FractionalOffset.bottomCenter,
          child: forgotLabel,
          color: Colors.transparent,
        ),
        Container(
          height: 80,
          width: MediaQuery.of(context).size.width * 0.47,
          //alignment: FractionalOffset.bottomCenter,
          child: registerLabel,
        )
        ,
      ],
      backgroundColor: Colors.transparent,
    );
  }
}

/*void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();

      // Email & password matched our validation rules
      // and are saved to _email and _password fields.
      _performLogin();
    }
    else{
      showInSnackBar('Please fix the errors in red before submitting.');

    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value)
    ));
  }
  void _performLogin() {
    // This is just a demo, so no actual login here.
    Navigator.push(context, MaterialPageRoute(builder: (context)=> Home_screen()));
  }

  _verticalD() => Container(
        margin: EdgeInsets.only(left: 10.0, right: 0.0, top: 0.0, bottom: 0.0),
      );

  }*/
